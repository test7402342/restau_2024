import React from 'react';
import { Link } from 'react-router-dom'; // Importez Link depuis react-router-dom
import './Home.css'; // Importez le fichier CSS pour le composant Home

function Home({ handleMenuClick }) {
  return (
    <div className="home">
      <div className='PreTitre'>De moi (lpb)</div>
      <div className="container">
        <div className="content">
          <h1>Joyeux anniversaire ♡</h1>
          {/* Utilisez Link pour rediriger vers la page /menu */}
          <Link to="/menu">
            <button>Clique ici !!!</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Home;
