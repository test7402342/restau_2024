// import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'; // Importez Routes
import ScrollToTopOnNavigation from './ScrollToTopOnNavigation'; // Importez le composant ScrollToTopOnNavigation

import Home from './Home';
import Menu from './Menu';
import AntillaisPage from './pages/Antillais';
import FrancaisPage from './pages/Francais';
import MexicainPage from './pages/Mexicain';
import ItalienPage from './pages/Italien';
import ChinoisPage from './pages/Chinois';
import JaponaisPage from './pages/Japonais';
import './App.css';

function App() {
  // const [page, setPage] = useState('home');

  const handleTypeClick = () => {
    // setPage('menu');
  };

  const handleBackToMenu = () => {
    // setPage('home');
  };

  return (
    <Router>
      <ScrollToTopOnNavigation />

      <div className="App">

        <Routes>
          <Route path="/" element={<Home handleMenuClick={handleTypeClick} />} />
          <Route path="/menu" element={<Menu handleTypeClick={handleTypeClick} handleBackToMenu={handleBackToMenu} />} />
          <Route path="/menu/antillais" element={<AntillaisPage />} />
          <Route path="/menu/francais" element={<FrancaisPage />} />
          <Route path="/menu/mexicain" element={<MexicainPage />} />
          <Route path="/menu/italien" element={<ItalienPage />} />
          <Route path="/menu/chinois" element={<ChinoisPage />} />
          <Route path="/menu/japonais" element={<JaponaisPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
