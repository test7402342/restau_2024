import React from 'react';
import restaurantImage1 from '../images/mexicain/piquin1.jpg';
import restaurantImage2 from '../images/mexicain/piquin2.jpg';
import restaurantImage3 from '../images/mexicain/two1.jpg';
import restaurantImage4 from '../images/mexicain/two2.jpg';
import restaurantImage5 from '../images/mexicain/gomex1.jpg';
import restaurantImage6 from '../images/mexicain/gomex2.jpg';
import restaurantImage7 from '../images/mexicain/mulli1.png';
import restaurantImage8 from '../images/mexicain/mulli2.jpg';
import './common.css';

function JaponaisPage() {
  return (
    <>
      {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Piquin</h1>
          <div className="restaurant-description">
          Dans le quartier de la Part-Dieu, entre la Place Danton et la rue Paul Bert, ce  restaurant va vous faire redécouvrir les vraies saveurs de cette gastronomie outre-atlantique. Une cadre évocateur et chaleureux
La jolie façade du restaurant cache un restaurant à la décoration contemporaine et soignée aux accents de ce pays coloré, plein de charme. Le dépaysement est ici annoncé, pour une aventure pleine de goût.
Une cuisine mexicaine maison par une chef de L'institut Paul Bocuse 
La cuisine a du caractère et défend la cuisine authentique mexicaine proposée en tapas et plats à la carte. Ici la cuisine est maison et réalisée à base de produits frais par de vrais défenseurs passionnés par leur culture et la gastronomie. Une jolie sélection de vin saura satisfaire les plus exigeants. 

        </div>
        <a href="https://www.piquin.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Two Amigos</h1>
          <div className="restaurant-description">
          Situé à proximité de la place Ampère dans le 2ème arrondissement de Lyon, Two Amigos est le nouveau restaurant en vogue du quartier !

Murs de pierre, plateaux de table en bois et chaises d'écolier (celles en bois et en acier !), le cadre est convivial et moderne. À l'image de son équipe, jeune et dynamique ! En plus d'être très agréable, le service est efficace.

Carte aux spécialités mexicaines
Une fois la carte en main, on retrouve les plats traditionnels de restaurants mexicains. À la différence près que Two Amigos fait preuve d'originalité dans les noms : "Give Me Love", "Kill Bill", "Two Hot Chicks"... même en cuisine le sens de l'humour est important !
        </div>
          <a href="https://twoamigos.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage3} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage4} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>     
      
       {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage5} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage6} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>GoMex Cantina</h1>
          <div className="restaurant-description">
          Chez GoMex Cantina, la viande est d’origine française, les produits sont frais et viennent de producteurs locaux ; une fraîcheur et une qualité que l’on retrouve dans les tacos comme dans les burritos qui sont à tomber ! Les épices viennent bien-sûr tout droit de Mexico, pour donner saveurs et authenticité à chaque recette.

Les amoureux de ceviche pourront aussi trouver leur bonheur, tout comme les végétariens qui pourront goûter aux nopales, les cactus riches en protéines que l’on retrouve dans les différentes recettes veggie de GoMex. Evidemment, toutes ces épices, ça donne soif : on arrose alors le repas d’une petite bière mexicaine, la Sol, goûteuse à souhait.


  </div>
          <a href="https://www.gomex-cantina.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Mulli</h1>
          <div className="restaurant-description">
          Mais connaissez-vous le nord de cette rue ? Cette zone qui n’est plus piétonne, qui n’est plus pavée, qui n’est pas mignonne et qui n’a pas de beaux porches renaissances de chaque côté pour vous accueillir. On y croise même un immeuble des années 80, des parkings moches et beaucoup de travaux en cours ces dernières années.
          Et voilà un de mes petits secrets de vieille Lyonnaise, les amis. Dans cette partie de la rue, se trouvent de bonnes adresses qui existent depuis fort longtemps et dont la qualité et le rapport qualité/prix n’est plus à prouver.
          </div>
          <a href="https://mulli.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage7} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage8} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
