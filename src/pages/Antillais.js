import React from 'react';
import restaurantImage1 from '../images/antillais/dakar1.jpg';
import restaurantImage2 from '../images/antillais/dakar2µ.jpg';
import restaurantImage3 from '../images/antillais/mangue1.webp';
import restaurantImage4 from '../images/antillais/mangue2.webp';
import restaurantImage5 from '../images/antillais/touareg1.jpg';
import restaurantImage6 from '../images/antillais/touareg2.jpg';
import restaurantImage7 from '../images/antillais/alexandria1.png';
import restaurantImage8 from '../images/antillais/alexandria2.png';
import './common.css';

function JaponaisPage() {
  return (
    <>
      {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Lyon Dakar</h1>
          <div className="restaurant-description">
          Lyon Dakar, c'est un restaurant sénégalais qui vous propose des saveurs inoubliables. Une invitation au voyage à travers des plats copieux et variés comme le bissap, le thiou ou encore le maffé de boeuf. Des produits de qualité, préparés avec soin par deux cuisinières sénégalaises. Ici, l’accueil est aussi généreux que la cuisine, puisque Samy, le patron, saura vous initier et vous conseiller lors du choix des plats. Quant à l'addition, celle-ci est assez douce avec un très bon rapport qualité/ prix. Une adresse qui vaut le détour ! On est parti pour l’Afrique...
        </div>
        <a href="https://www.lyonresto.com/restaurant-Lyon/restaurant-Lyon-Dakar-Lyon/restaurant-Lyon-Dakar-Lyon-334.html" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>La Mangue Amère</h1>
          <div className="restaurant-description">
          Si vous êtes à la recherche d'une expérience culinaire exotique à Lyon, La Mangue Amère est l'endroit idéal pour satisfaire votre appétits. Situé au cœur de la ville, ce restaurant propose une cuisine sénégalaise et antillaise pour éveiller vos papilles et vous emmènera dans un voyage gustatif inoubliable.

  </div>
          <a href="https://la-mangue-amere-restaurant-lyon.eatbu.com/?lang=fr" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage3} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage4} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>     
      
       {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage5} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage6} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Le Touareg</h1>
          <div className="restaurant-description">
          Dans l'assiette : l'inspiration vient de l'Orient et du Maghreb. En entrée, on retrouve ainsi mezze et kamia pour suivre avec le couscous parfumé, tajine sucré-salé, thé brulant et pâtisseries maisons enchanteront les amoureux de bonne cuisine. Mais la spécialité de Naîma c'est aussi l'audace, toujours rajouter un petit quelque chose qui n'était pas dans la recette : coriandre, menthe fraîche... Son plat fétiche ? Le tajine de bœuf aux pistils de safran, pomme et sésame. Cuisiné avec de l'eau de rose, de la cardamone il régale les plus gourmets.

  </div>
          <a href="https://letouareg.fr/fr" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Alexandria</h1>
          <div className="restaurant-description">
          À la base du restaurant, une famille adorable, originaire d’Alexandrie. Installés à Lyon depuis 10 ans, père, mère, fils et fille se sont unis pour nous faire découvrir les saveurs de leur pays. Tout le monde y a mis du sien dans ce projet, sachant que l’expertise du fils aîné (6 années de restauration et ancien gérant de bar à tapas) -il est également le gérant principal et l’associé d’Alexandria- et l’expérience de la fille benjamine dans l’hôtellerie ont apporté leur pierre à l’édifice. 
          </div>
          <a href="https://www.lebonbon.fr/lyon/cuisine-du-monde/alexandria-premier-restaurant-egyptien-lyon/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage7} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage8} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
