import React from 'react';
import restaurantImage1 from '../images/français/voile1.avif';
import restaurantImage2 from '../images/français/voile2.avif';
import restaurantImage3 from '../images/français/chez2.jpg';
import restaurantImage4 from '../images/français/chez1.jpg';
import restaurantImage5 from '../images/français/poulel1.jpg';
import restaurantImage6 from '../images/français/poule2.jpg';
import restaurantImage7 from '../images/français/raph1.jpg';
import restaurantImage8 from '../images/français/raph2.webp';
import './common.css';

function JaponaisPage() {
  return (
    <>
      {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Les Petites Voiles</h1>
          <div className="restaurant-description">
          Les Petites Voiles, c’est un lieu exceptionnel, avec une vue dégagée sur la Saône en pleine nature. À deux, entre amis ou en famille, rien de mieux pour partager et se détendre. Si vous venez en bateau vous pourrez même accoster au ponton du restaurant. Et s’il pleut ? Pas de panique, le restaurant dispose d’une terrasse couverte et d’une salle intérieure. Décorée avec des tableaux colorés, les lumières sont tamisées, et la grande baie vitrée vous offre une vue magique sur la Saône, il y a une ambiance chaleureuse et décontractée.

        </div>
        <a href="https://www.lespetitesvoiles.com/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Chez Grand-Mère</h1>
          <div className="restaurant-description">
          Je vous propose que l’on se retrouve tous chez Grand-Mère ! Situés sur le passage pavé d’une rue piétonne, rue du Bœuf du vieux Lyon, Marilyn et son équipe se démènent chaque jour pour nous accueillir dans leur restaurant chez Grand-Mère, et nous replongent dans l’atmosphère cocooning et réconfortante d’un repas convivial chez Mamie. Une équipe dévouée et familiale ou vous vous sentirez comme à la maison. Le bouchon a été pensé afin que l’on déconnecte en repensant à nos moments en famille. Des recettes plus appétissantes les unes que les autres ainsi que le cadre chaleureux avec les murs parsemés de pierres vous procureront l’effet de vous sentir comme chez vous.
        </div>
          <a href="https://chezgrandmere.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage3} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage4} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>     
      
       {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage5} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage6} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Daddy Poule</h1>
          <div className="restaurant-description">
          Daddy Poule c’est l’adresse atypique de Tassin-la-Demi-Lune. Un concept pensé par le chef Philippe Gauvreau, qui offre à ses clients un lieu unique sur les toits de la ville où on peut déguster une cuisine fraiche et savoureuse tout en étant élégante. Entre simplicité et convivialité, découvrez ce restaurant unique en son genre.

  </div>
          <a href="https://www.daddy-poule.com/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Raphaël</h1>
          <div className="restaurant-description">
            Découvrez Chez Raphaël - Le Restaurant et Les Bons Copains de Raphaël : deux adresses incontournables à Lyon.<br/>
            Chez Raphaël - Le Restaurant offre une cuisine généreuse et conviviale, parfaite pour les déjeuners d’affaires en semaine et les repas entre amis. Idéalement situé au cœur du 6ème arrondissement de Lyon, près du Parc de la Tête d’or. Les Bons Copains de Raphaël, quant à eux, proposent des Tapas, vins et cocktails jusqu'à 01h00 du matin, du Lundi au Samedi. Un lieu idéal pour vos soirées d'entreprise et Afterwork.
          </div>
          <a href="https://www.chez-raphael.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage7} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage8} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
