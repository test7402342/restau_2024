import React from 'react';
import restaurantImage1 from '../images/italien/scala.webp';
import restaurantImage2 from '../images/italien/scala2.jpg';
import restaurantImage3 from '../images/italien/carmelo1.jpg';
import restaurantImage4 from '../images/italien/carmelo2.jpg';
import restaurantImage5 from '../images/italien/antonio1.webp';
import restaurantImage6 from '../images/italien/antonio2.jpg';
import restaurantImage7 from '../images/italien/cugini1.jpg';
import restaurantImage8 from '../images/italien/cugini2.jpg';
import './common.css';

function JaponaisPage() {
  return (
    <>
      {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>La Scala</h1>
          <div className="restaurant-description">
          Direction le 3e et la belle histoire familiale de La Scala Siciliana. Ayant obtenu le prestigieux label d’excellence « Ospitalita Italiana », ce restaurant tenu par deux frangins d’origines siciliennes (hyper accueillants d’ailleurs) propose une cuisine traditionnelle de leur île. Assiettes de charcuterie délicieuses, salades hyper-généreuses, pappardelle à la crème de truffe, polpette alla Siciliana… La carte regorge de merveilles culinaires inconnues de nos papilles françaises. Si on adore absolument tout, la mention spéciale revient aux pizzas et au tiramisu maison qui sont vraiment à tomber… Pour couronner le tout, une jolie petite terrasse dans une rue bien calme.
        </div>
        <a href="https://antoefabio.com/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Carmelo</h1>
          <div className="restaurant-description">
          Installé à Lyon depuis début 2020, Carmelo n'est autre que le premier restaurant lyonnais du groupe Big Mamma. Alors forcément, la déco est incroyable, les plats sont préparés avec des produits sourcés finement sélectionnés aux quatres coins de l'Italie et toute l'équipe est italienne et parle avec ce bel accent chantant. Une immersion totale au pays de la bonne bouffe et des bons cocktails avec un rapport qualité-prix IM-BAT-TABLE !
        </div>
          <a href="https://www.bigmammagroup.com/fr/trattorias/carmelo" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage3} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage4} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>     
      
       {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage5} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage6} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Antonio & Marco</h1>
          <div className="restaurant-description">
          Chez Antonio & Marco, on retrouve tous les trésors de la Botte : des panozzos, antipastis, des arancinis et des cannolos sicilianis préparés avec des produits directement importés d’Italie sans oublier des pizzas napolitaines avec une pâte maison réalisée avec leur propre levain (fermentation de 72h) et de la farine bio. En plus des saveurs, le voyage promet d’être aussi total en salle puisque l’effectif 100% italien et une ambiance survoltée du soir au matin. Bref, la vraie belle énergie italienne dans toute sa splendeur !

  </div>
          <a href="https://antoniomarcopizzeria.com/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Cugini</h1>
          <div className="restaurant-description">
          Cugini par le chef Tomas Parisini, premier concept de cuisine franco-italienne en France ! Pour Tomas, la cuisine est une affaire de famille. Fils de restaurateurs, ce chef talentueux affiche une détermination sans faille pour faire briller la gastronomie italienne et française. En témoigne les nombreuses distinctions qu’il a reçu, Tomas distille son savoir-faire pour les clients impatients de découvrir la générosité de sa cuisine.

          </div>
          <a href="https://www.cugini-restaurant.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage7} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage8} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
