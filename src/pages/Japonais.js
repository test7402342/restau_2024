import React from 'react';

import sushido1 from '../images/japonais/sushido1.jpg';
import sushido2 from '../images/japonais/sushido2.jpg';
import haru1 from '../images/japonais/haru1.webp';
import haru2 from '../images/japonais/haru2.webp';
import yuzuya1 from '../images/japonais/yuzuya.jpeg';
import yuzuya2 from '../images/japonais/yuzuya2.jpeg';
import mubyotan1 from '../images/japonais/mubyotan1.jpg';
import mubyotan2 from '../images/japonais/mubyotan2.jpg';

import './common.css';

function JaponaisPage() {
  return (
    <>
      <div className="japonais-page">
        <div className="image-container1">
          {mubyotan1 && <img src={mubyotan1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {mubyotan2 && <img src={mubyotan2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1 className="restaurant-name">Mubyotan</h1>
          <div className="restaurant-description">
            Bienvenue au Mubyotan, petit morceau de Japon à la Croix-Rousse. Nous sommes une teishoku-ya, cantine dédiée à une cuisine japonaise populaire, familiale et entièrement réalisée par nos soins. « Mubyotan », littéralement « les six calebasses », peut aussi se dire mubyô 六瓢, mot qui évoque par sa sonorité une expression chérie des Japonais : mubyô-sokusai 無病息災, conjurer la maladie et s’assurer une bonne santé.
          </div>
          <a href="https://mubyotan.fr/" className="visit-button" target="_blank" rel="noreferrer">Visiter le restaurant</a>
        </div>
      </div>

      <div className="japonais-page">
        <div className="restaurant-details">
          <h1 className="restaurant-name">Yuzuya Izakaya</h1>
          <div className="restaurant-description">
            L'Izakaya est célèbre et répandu au Japon comme le sont les bars à tapas en Espagne ou les bistrots en France. C'est un lieu convivial où l'on s'attarde pour déguster une bière, un saké (nihonshu), du shochû (alcool de patate douce) ou un cocktail accompagné de plats divers et variés. Nous avons souhaité partager avec vous cette expérience ; c'est de là qu'est né Yuzuya, après avoir quelque peu revisité le concept de l'izakaya...
          </div>
          <a href="https://yuzuya-izakaya-bar-lyon.fr/fr" className="visit-button" target="_blank" rel="noreferrer">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {yuzuya1 && <img src={yuzuya1} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {yuzuya2 && <img src={yuzuya2} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>

      <div className="japonais-page">
        <div className="image-container1">
          {haru1 && <img src={haru1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {haru2 && <img src={haru2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1 className="restaurant-name">Haru</h1>
          <div className="restaurant-description">
            "Haru Sushi Bar est le fruit de la passion de l’art du Sushi Japonais. Un restaurant gastronomique met à l’honneur des plats de qualité réalisés par des chefs cuisiniers dotés de plusieurs années d’expérience."
          </div>
          <a href="https://harusushibar.fr/fr" className="visit-button" target="_blank" rel="noreferrer">Visiter le restaurant</a>
        </div>
      </div>

      <div className="japonais-page">
        <div className="restaurant-details">
          <h1 className="restaurant-name">Sushido</h1>
          <div className="restaurant-description">
            Le restaurant japonais comme on l'aime. Le Sushido propose un sympathique choix de plats asiatiques : sushis au saumon, au thon ou à la daurade, maki au saumon, au thon, au surimi ou au cream cheese, brochettes, soupes... Il y en a vraiment pour tous les goûts. Les produits proposés sont frais et de qualité et, crevette sur le maki, les plats sont en self-service, présentés sur un tapis roulant. Chose rare chez un japonais, le restaurant propose aussi un petit choix de desserts. Côté ambiance, la salle est dôtée d'une décoration épurée et simple, mais néanmoins agréable.
          </div>
          <a href="https://www.google.fr/search?q=Sushido&sca_esv=5d95fa5235c39ff3&sca_upv=1&sxsrf=ACQVn08Gsy8UKHpvxlcSroj5TpwQ-K5E6w%3A1709584525369&source=hp&ei=jTDmZYHxE9ighbIPjZOc6AY&iflsig=ANes7DEAAAAAZeY-nX_WzTYik4abdYGiiPcORGDpUeAb&ved=0ahUKEwjB6-HSutuEAxVYUEEAHY0JB20Q4dUDCA4&uact=5&oq=Sushido&gs_lp=Egdnd3Mtd2l6IgdTdXNoaWRvMhMQLhgUGK8BGMcBGIcCGIAEGI4FMggQABiABBixAzIFEAAYgAQyBRAAGIAEMgsQLhiABBjHARivATILEC4YgAQYxwEYrwEyCxAuGIAEGMcBGK8BMgUQABiABDIOEC4YgAQYxwEYrwEYjgUyCxAuGIAEGMcBGK8BSIERUJEMWJEMcAF4AJABAJgB3wGgAd8BqgEDMi0xuAEDyAEA-AEC-AEBmAICoAKJAqgCCsICBxAjGOoCGCfCAhAQLhjHARivARiOBRjqAhgnmAMVkgcFMS4wLjGgB44S&sclient=gws-wiz" className="visit-button" target="_blank" rel="noreferrer">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {sushido2 && <img src={sushido2} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {sushido1 && <img src={sushido1} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
