import React from 'react';
import restaurantImage1 from '../images/chinois/panda1.jpg';
import restaurantImage2 from '../images/chinois/panda2.png';
import restaurantImage3 from '../images/chinois/engimono1.jpg';
import restaurantImage4 from '../images/chinois/engimono2.jpg';
import restaurantImage5 from '../images/chinois/grain1.png';
import restaurantImage6 from '../images/chinois/grain2.jpg';
import restaurantImage7 from '../images/chinois/yummy1.jpg';
import restaurantImage8 from '../images/chinois/yummy2.jpg';
import './common.css';

function JaponaisPage() {
  return (
    <>
      {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage1} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage2} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Panda Chop's</h1>
          <div className="restaurant-description">
          La carte de Panda Chop’s est le reflet d'un véritable voyage gustatif à travers la Chine. Les 6 plats de la carte représentent une région spécifique du pays, offrant ainsi une découverte inédite de saveurs : une carte courte mais de qualité. Pas question de composer des plats avec des produits surgelés ! Tout est frais et fait maison : de la marinade du poulet jusqu’à la pâte à ravioli, le mot d’ordre est savoir-faire ! L’équipe fait tout dans les règles de l’art, en respectant soigneusement les traditions.

        </div>
        <a href="https://www.pandachops.com/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Engimono</h1>
          <div className="restaurant-description">
          Engimono, du chinois «Les Cinq Grains, sont un groupe de cinq plantes cultivées qui étaient toutes importantes dans la Chine ancienne » signifie « Bonheur ». Engimono a ouvert ses portes le 8 avril 2013 et propose un voyage culinaire de la Chine et ouvre une véritable fenêtre sur la Chine en plein cœur de Lyon. Sur la carte, un éventail de plus de 60 plats se déploie pour vous faire découvrir des saveurs exotiques d’une grande subtilité. Le savoir-faire traditionnel est assuré par le Chef qui orchestre une brigade chinoise comptant un Maître dans chaque matière : le Wok, le Chopper (la découpe) et les Dim Sum.

        </div>
          <a href="https://www.restaurantengimono.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage3} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage4} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>     
      
       {/* Premier restaurant */}
      <div className="japonais-page">
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage5} alt="Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage6} alt="Restaurant 2" className="restaurant-image" />}
        </div>
        <div className="restaurant-details">
          <h1>Le petit grain</h1>
          <div className="restaurant-description">
          Découvrez le charme de l’Asie au Petit Grain, où chaque repas est une expérience unique. De 11h à tard le soir, notre menu unique vous emmène d’un déjeuner vietnamien authentique avec des dim sums délicats et des bobuns traditionnels à des soirées envoûtantes avec des Baos fondants et du Pho réconfortant. Partagez des moments de convivialité autour de notre formule de dégustation, complétés par des cocktails exotiques et des desserts gourmands. Que ce soit sur notre terrasse ensoleillée ou dans notre salle chaleureuse, venez vivre un voyage culinaire inoubliable à Lyon.
  </div>
          <a href="https://le-petit-grain-lyon.fr/" className="visit-button">Visiter le restaurant</a>
        </div>
      </div>

      {/* Deuxième restaurant (inversé) */}
      <div className="japonais-page">
        <div className="restaurant-details">
          <h1>Yummy Yummy</h1>
          <div className="restaurant-description">
          A volonté  !!! Dans ma fondue chinoise il y a un bouillon épicé. Dans ma fondue chinoise il y a un bouillon épicé et des nouilles. Dans ma fondue chinoise il y a un bouillon épicé, des nouilles, du bœuf, du tofu, du chou et bien d'autres ingrédients. Dans ma fondue chinoise, je mets tout ce que je trouve sur ce tapis roulant qui défile sous mes yeux. C'est à volonté et je ne vais pas me priver.
            </div>
          <a href="https://www.petitpaume.com/etablissements/yummy-yummy" className="visit-button">Visiter le restaurant</a>
        </div>
        <div className="image-container2">
          {restaurantImage2 && <img src={restaurantImage7} alt="Deuxième Restaurant 1" className="restaurant-image" />}
        </div>
        <div className="image-container1">
          {restaurantImage1 && <img src={restaurantImage8} alt="Deuxième Restaurant 2" className="restaurant-image" />}
        </div>
      </div>
    </>
  );
}

export default JaponaisPage;
