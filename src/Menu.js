import React from 'react';
import { Link } from 'react-router-dom'; // Importez Link depuis react-router-dom
import './Menu.css'; // Importez le fichier CSS pour le composant Menu

function Menu({ handleTypeClick }) {
  // Liste des types de cuisines avec leurs descriptions et images
  const cuisines = [
    { id: 1, name: "Japonais", description: "Plutôt japonais ?", image: "/japonais.jpg", link: "/menu/japonais" },
    { id: 2, name: "Italien", description: "Ou bien italien ?", image: "/italien.jpg", link: "/menu/italien" },
    { id: 3, name: "Antillais", description: "Ou encore Africain ?", image: "/antillais.jpg", link: "/menu/antillais" },
    { id: 4, name: "Chinois", description: "Une envie de chinois ?", image: "/chinois.jpg", link: "/menu/chinois" },
    { id: 5, name: "Français", description: "Peut-être de la cuisine française ?", image: "/français.jpg", link: "/menu/francais" },
    { id: 6, name: "Espagnol", description: "Des tapas du Mexique ?", image: "/mexicain.jpg", link: "/menu/mexicain" },
  ];

  return (
    <div className="menu">
      <div className='sections'>
        <div className="left-section">
          <h1 className="title">QUELLE ENVIE AS-TU ?</h1>
          <p className="description">Pour ton anniversaire tu peux choisir entre tous ces types de cuisines puis choisir un restaurant qui te donne envie :)</p>
        </div>
        <div className="right-section">
          {cuisines.map(cuisine => (
            <Link key={cuisine.id} to={cuisine.link} className="cuisine-block" onClick={() => handleTypeClick(cuisine.id)}>
              <img src={process.env.PUBLIC_URL + cuisine.image} alt={cuisine.name} />
              <p>{cuisine.description}</p>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Menu;
